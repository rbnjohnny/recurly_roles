<?php

/**
 * @file
 * Assign users a role based on a Recur.ly subscription.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\recurly\Controller\RecurlySubscriptionSelectPlanController;
use Drupal\user\Entity\User;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function recurly_roles_form_recurly_settings_form_alter(&$form, FormStateInterface $form_state) {
  $settings = Drupal::config('recurly_roles.settings');

  // If the 'user' entity type is selected for syncing lets add some options.
  $form['sync']['recurly_roles'] = [
    '#type' => 'fieldset',
    '#title' => t('Roles to apply to user account'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => [
      'visible' => [
        ':input[name="recurly_entity_type"]' => ['value' => 'user'],
      ],
    ],
  ];
  // Users can select a plan when registering if the account settings are
  // configured to allow immediate sign-in on successful registration.
  $canSelectPlanOnRegister = !Drupal::config('user.settings')->get('verify_mail') && Drupal::config('user.settings')->get('register') == 'visitors';
  $selectPlanOnRegisterDisabled = !$canSelectPlanOnRegister ? t('This option is disabled since the account settings are not configured to allow immediate sign-in on successful registration.') : '';
  $form['sync']['recurly_roles_enabled_on_registration_form'] = [
    '#type' => 'checkbox',
    '#title' => t('Display subscription options on user registration form.'),
    '#description' => t('Enabling this option will display a list of subscription plans on the user registration form, allowing users to choose a plan while registering for their account. %disabled', ['%disabled' => $selectPlanOnRegisterDisabled]),
    '#default_value' => $canSelectPlanOnRegister ? $settings->get('enabled_on_registration_form') : FALSE,
    '#states' => [
      'visible' => [
        ':input[name="recurly_entity_type"]' => ['value' => 'user'],
      ],
    ],
    '#attributes' => ['disabled' => !$canSelectPlanOnRegister ? 'disabled' : FALSE],
  ];
  $form['sync']['recurly_roles_enabled_on_registration_form_required'] = [
    '#type' => 'checkbox',
    '#title' => t("Require users to choose a subscription plan in order to complete registration."),
    '#default_value' => $settings->get('enabled_on_registration_form_required'),
    '#states' => [
      'visible' => [
        ':input[name="recurly_entity_type"]' => ['value' => 'user'],
        ':input[name="recurly_roles_enabled_on_registration_form"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['sync']['recurly_roles']['help'] = [
    '#type' => 'markup',
    '#markup' => t("<p>Using the options below you can choose to add/remove roles from a user's account based on their Recurly subscription status. Global settings apply to all users regardless of which plan they are subscribed to.</p>"),
  ];

  // Add a set of fields for choosing a roles to add/remove for any plan.
  $form['sync']['recurly_roles']['default'] = _recurly_roles_form_fields('default');
  $form['sync']['recurly_roles']['default']['#title'] = t('Global Settings - Applies to All Plans');

  // Add a set of fields to choose roles to add/remove for each plan.
  $plans = Drupal::config('recurly.settings')
    ->get('recurly_subscription_plans') ?: [];

  $plan_codes = array_keys($plans);

  if (count($plan_codes)) {
    foreach ($plan_codes as $plan_code) {
      $form['sync']['recurly_roles'][$plan_code] = _recurly_roles_form_fields($plan_code);
    }
  }

  // List of plans to save in submit callback.
  $form_state->set('recurly_roles_plans', array_merge($plan_codes, ['default']));

  $form['#submit'][] = 'recurly_roles_settings_form_submit';
}

/**
 * Builds Recurly Roles selection checkboxes.
 *
 * Build a set of checkboxes for choosing role(s) to add/remove when a user
 * subscribes to a plan, or their current subscription expires.
 *
 * @param string $plan
 *   Recurly plan name.
 *
 * @return array
 *   Form API array.
 */
function _recurly_roles_form_fields($plan) {
  $plan_roles = Drupal::config('recurly_roles.plans.' . $plan);
  $roles = user_role_names(TRUE);

  $fields = [
    '#type' => 'fieldset',
    '#title' => $plan,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $fields['recurly_roles_plans_' . $plan . '_add'] = [
    '#type' => 'checkboxes',
    '#title' => t('Roles to add to an account when the %plan plan is purchased', ['%plan' => $plan]),
    '#options' => $roles,
    '#default_value' => $plan_roles->get('add') ?: [],
  ];

  $fields['recurly_roles_plans_' . $plan . '_remove'] = [
    '#type' => 'checkboxes',
    '#title' => t('Roles to remove from accounts when the %plan plan expires.', ['%plan' => $plan]),
    '#options' => $roles,
    '#default_value' => $plan_roles->get('remove') ?: [],
  ];

  $fields['recurly_roles_plans_' . $plan . '_expired_add'] = [
    '#type' => 'checkboxes',
    '#title' => t('Roles to add to an account when the %plan plan expires.', ['%plan' => $plan]),
    '#options' => $roles,
    '#default_value' => $plan_roles->get('expired_add') ?: [],
  ];

  return $fields;
}

/**
 * Form submit callback.
 */
function recurly_roles_settings_form_submit(array &$form, FormStateInterface $form_state) {
  Drupal::configFactory()->getEditable('recurly_roles.settings')
    ->set('enabled_on_registration_form', $form_state->getValue('recurly_roles_enabled_on_registration_form'))
    ->set('enabled_on_registration_form_required', $form_state->getValue('recurly_roles_enabled_on_registration_form_required'))
    ->save();

  foreach ($form_state->get('recurly_roles_plans') as $plan) {
    Drupal::configFactory()->getEditable('recurly_roles.plans.' . $plan)
      ->set('add', array_keys(array_filter($form_state->getValue('recurly_roles_plans_' . $plan . '_add'))))
      ->set('remove', array_keys(array_filter($form_state->getValue('recurly_roles_plans_' . $plan . '_remove'))))
      ->set('expired_add', array_keys(array_filter($form_state->getValue('recurly_roles_plans_' . $plan . '_expired_add'))))
      ->save();
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function recurly_roles_form_user_register_form_alter(&$form, FormStateInterface &$form_state) {
  // Do not add these additions to the user register form when it's being shown
  // at admin/people/create.
  if (Drupal::routeMatch()->getRouteName() === 'user.admin_create') {
    return;
  }

  // Optionally add subscription options to the user registration form.
  if (Drupal::config('recurly_roles.settings')->get('enabled_on_registration_form')
    && !Drupal::config('user.settings')->get('verify_mail')
    && Drupal::config('user.settings')->get('register') == 'visitors') {
    $form['recurly_subscription_plans'] = _recurly_roles_plan_options();

    // Add submit handler.
    foreach (array_keys($form['actions']) as $action) {
      if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
        $form['actions'][$action]['#submit'][] = 'recurly_roles_user_register_form_submit';
      }
    }
  }
}

/**
 * Form submission handler for user_register_form.
 *
 * Redirects to recurly signup page.
 *
 * @see recurly_roles_form_user_register_form_alter()
 */
function recurly_roles_user_register_form_submit($form, FormStateInterface &$form_state) {
  if ($form_state->getValue('recurly_roles_radios')) {
    $form_state->setRedirect('entity.user.recurlyjs_signup', [
      'operation' => 'signup',
      'plan_code' => $form_state->getValue('recurly_roles_radios'),
      'user' => $form_state->getValue('uid'),
    ]);
  }
}

/**
 * Generate a list of enabled Recurly plans.
 *
 * Includes a set of radio buttons, one for each plan and a formatted display
 * of the plan name and information along with some Javascript to allow hiding
 * the radio buttons and using the displayed plan name to toggle the buttons.
 *
 * @return array
 *   A render array that contains a set of radio buttons and the list of plans.
 */
function _recurly_roles_plan_options() {
  $recurlySettings = Drupal::config('recurly.settings');

  $currency = $recurlySettings->get('recurly_default_currency') ?: 'USD';

  $all_plans = recurly_subscription_plans();
  $enabled_plan_keys = $recurlySettings->get('recurly_subscription_plans') ?: [];
  $enabled_plans = [];
  foreach ($enabled_plan_keys as $plan_code => $plan_info) {
    foreach ($all_plans as $plan) {
      if ($plan_info['status'] == '1' && $plan_code == $plan->plan_code) {
        $enabled_plans[$plan_code] = $plan;
      }
    }
  }

  $build = [];
  $build['recurly_roles_radios'] = [
    '#type' => 'radios',
    '#title' => t('Choose a plan'),
    '#options' => array_map(function ($plan) {
      return $plan->name;
    }, $enabled_plans),
    '#required' => Drupal::config('recurly_roles.settings')
      ->get('enabled_on_registration_form_required'),
  ];

  $build['recurly_roles_options'] = [
    '#theme' => [
      'recurly_subscription_plan_select__' . RecurlySubscriptionSelectPlanController::SELECT_PLAN_MODE_SIGNUP,
      'recurly_subscription_plan_select',
    ],
    '#plans' => $enabled_plans,
    '#entity_type' => 'user',
    '#entity' => User::getAnonymousUser(),
    '#currency' => $currency,
    '#mode' => RecurlySubscriptionSelectPlanController::SELECT_PLAN_MODE_SIGNUP,
    '#subscriptions' => [],
    '#subscription_id' => NULL,
  ];

  // Add the JS we need.
  $build['#attached']['library'][] = 'recurly_roles/recurly_roles.registration';

  return $build;
}

/**
 * Implements hook_recurly_process_push_notification().
 */
function recurly_roles_recurly_process_push_notification($subdomain, $notification) {
  $entity_type = Drupal::config('recurly.settings')
    ->get('recurly_entity_type');

  if ($entity_type == 'user') {
    $account = recurly_account_load(['account_code' => $notification->account->account_code], TRUE);
    $account = Drupal::entityTypeManager()
      ->getStorage('user')
      ->load($account->entity_id);
    switch ($notification->type) {
      // Add the subscriber role.
      case 'new_subscription_notification':
      case 'reactivated_account_notification':
        _recurly_roles_add_roles($account, $notification->subscription->plan->plan_code);
        break;

      case 'updated_subscription_notification':
        // Remove default roles and roles for all plans.
        _recurly_roles_remove_roles($account, $notification->subscription->plan->plan_code);
        // Add the roles for the new plan.
        _recurly_roles_add_roles($account, $notification->subscription->plan->plan_code);
        break;

      // Remove the subscriber role.
      case 'expired_subscription_notification':
        _recurly_roles_remove_roles($account, $notification->subscription->plan->plan_code);
        _recurly_roles_add_roles($account, $notification->subscription->plan->plan_code, 'expired');
        break;

      // @todo: Should we do something with failed payments? Like flag the
      // account so we can display something different on the dashboard?
    }
  }
}

/**
 * Helper function to add roles to an account.
 *
 * @param object $account
 *   User account object to which the roles should be added.
 * @param string $plan_code
 *   The plan_code for the plan to add roles for.
 * @param string $reason
 *   Why roles are being added, either 'expired' or 'subscribe'.
 *
 * @return mixed
 *   TRUE if one or more roles are added, NULL otherwise.
 */
function _recurly_roles_add_roles($account, $plan_code, $reason = 'subscribe') {
  $role_set_name = ($reason === 'expired') ? 'expired_add' : 'add';

  // Get default add roles.
  $add_roles = Drupal::config('recurly_roles.plans.default')->get($role_set_name);
  // Combine our global add roles with the plan's roles to add.
  $add_roles = array_merge($add_roles, Drupal::config('recurly_roles.plans.' . $plan_code)->get($role_set_name), []);

  $add_roles = array_filter($add_roles);

  $added_roles = 0;
  if (count($add_roles)) {
    foreach ($add_roles as $rid) {
      if (!$account->hasRole($rid)) {
        $added_roles++;
        $account->addRole($rid);
      }
    }
  }
  if ($added_roles > 0) {
    $account->save();
    return TRUE;
  }
  return NULL;
}

/**
 * Helper function to remove roles from an account.
 *
 * @param object $account
 *   User object representing the account to which roles should be applied.
 * @param string $plan_code
 *   The plan_code for the plan to add roles for.
 *
 * @return mixed
 *   Returns TRUE if one or more roles are removed from the account. If no
 *   roles were removed from the $account NULL is returned.
 */
function _recurly_roles_remove_roles($account, $plan_code) {
  // Get default remove roles.
  $remove_roles = Drupal::config('recurly_roles.plans.default')->get('remove');
  // Combine our global remove roles with the plan's roles to remove.
  $remove_roles = array_merge($remove_roles, Drupal::config('recurly_roles.plans.' . $plan_code)->get('remove'), []);

  $remove_roles = array_filter($remove_roles);
  $removed_roles = 0;
  if (count($remove_roles)) {
    foreach ($remove_roles as $rid) {
      if ($account->hasRole($rid)) {
        $removed_roles++;
        $account->removeRole($rid);
      }
    }
  }
  if ($removed_roles > 0) {
    $account->save();
    return TRUE;
  }
  return NULL;
}
